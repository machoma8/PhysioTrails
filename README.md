# PhysioTrails

PhysioTrails is an application for virtual reality serving as an auxiliary tool for balance training. The   system consists of two applications, a VR application mediating a travelling experience for Oculus Quest and an Android mobile application for controlling the VR experience.


**Relevant** **Branches**

- *vr-app*: Unity project set for building the VR application
- *mobile-app*: Unity project set for building the mobile application


**Content**

- source project of the thesis in Unity 3D
- ApplicationExports folder:

    — PhysioTrails-vr.apk - installation package of the VR application

    — PhysioTrails-mobile.apk - installation package of the mobile application

---

## Installation Manual

### VR Application

**HW Requirements**

- Oculus Quest 1 or 2


**SW Requirements**

- SideQuest (or other application for side-loading APK files to Quest devices)


**Installation**

1. Clone this repo
2. Connect the Quest device to your computer and make sure it is switched on
3. Open SideQuest and drag and drop the *PhysioTrails-vr.apk* from the *ApplicationExports* folder into the SideQuest window
4. Enable hand tracking in the Quest device
5. Navigate to Applications/Unknown Sources in the Quest device
6. Open the PhysioTrails application


### Mobile Application

**HW Requirements**

- Android mobile device with Android OS 6 or above
- Wi-Fi connection and Oculus Quest 1 or 2 with PhysioTrails application running


**SW Requirements**

- Android File Transfer (or other application for side-loading APK files to Android mobile devices)


**Installation**

1. Clone this repo
2. Connect the Android device to your computer and make sure it is switched on
3. Open Android File Transfer and drag and drop the *PhysioTrails-mobile.apk* from the *ApplicationExports* folder into the Android File Transfer window
4. Navigate to Settings/Storage/Explore in the Android device
5. Open the PhysioTrails application package and choose Install
6. Open the PhysioTrails application


 > For a more complex installation manual, please see the Appendix B in the thesis' text.
